// ==UserScript==
// @name         newtabSearch
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.google.co.jp/search*
// @match        https://www.google.co.jp/?gws_rd=ssl#q=*
// @grant        none
// ==/UserScript==
function filterHaveInnerHTML(argElements) {
  var i;
  var elms = [];
  for(i=0;i < argElements.length;i++){
      var element = argElements[i];
      if(element.innerHTML !== "") {
          elms.push(element);
      }
  }
   return elms;
}


function paintAll(argElements) {
  var i;
  for(i=0;i < argElements.length;i++){
      var element = argElements[i];
      element.style.backgroundColor = "#FCDEFA";
      //element.style.backgroundColor = "#D8F2F2";
      element.style.color = "#060C0C";
  }
}

function paintSingle(number, argElements) {
  var nowElement = argElements[number];
  nowElement.style.outline = "none";
  nowElement.style.backgroundColor = " #F213E3";
// nowElement.style.backgroundColor = "#1B18F2";
  nowElement.style.color = "#fff";
  nowElement.focus();
}

var elements = filterHaveInnerHTML(document.querySelectorAll("h3>a"));
var now = -99;

paintAll(elements);
document.addEventListener("keydown", function(event) {
  if(now == -99) {
    if(event.keyCode == 38 || event.keyCode == 40) {
      now = 0;
      paintSingle(now, elements);
    }
  } else {
    if (event.keyCode == 38) {
      if (now > 0) {
        now = now - 1;
      }
      paintAll(elements);
      paintSingle(now, elements);
    }
    if (event.keyCode == 40) {
      if (now < elements.length - 1) {
        now = now + 1;
      }
      paintAll(elements);
      paintSingle(now, elements);
    }
  }
});
