// ==UserScript==
// @name         selectedSearch
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        *://*/*
// @grant        none
// ==/UserScript==

var keyStatus = {};
var keyMeta = false;

function getSelectionParentElement() {
    var parentEl = null, sel;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount) {
            parentEl = sel.getRangeAt(0).commonAncestorContainer;
            if (parentEl.nodeType != 1) {
                parentEl = parentEl.parentNode;
            }
        }
    } else if ( (sel = document.selection) && sel.type != "Control") {
        parentEl = sel.createRange().parentElement();
    }
    return parentEl;
}

function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}



document.addEventListener("keydown", function(event) {
    keyMeta = event.metaKey;
    keyStatus[event.keyCode] = true;
    if(keyMeta && keyStatus[13]) {
        if (getSelectionText() !== "") {
            var para = document.createElement("a");
            var node = document.createTextNode(getSelectionText());
            para.appendChild(node);
            para.setAttribute('href', 'http://google.com/search?q='+getSelectionText());
            getSelectionParentElement().appendChild(para);
            para.focus();
            window.getSelection.selectedIndex = 0;
        }
    }
});

document.addEventListener("keyup", function(event) {
    keyMeta = false;
    keyStatus[13] = false;
    keyStatus[event.keyCode] = false;
});
